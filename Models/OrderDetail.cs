﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EShop.Models
{
    public class OrderDetail
    {
        public int Id { get; set; }

        public int ProductId {get;set;}

        public Product Product { get; set; }

        public int OrderId { get; set; }

        public Order Order { get; set; }

        [Required]
        public int UnitPrice { get; set; }

        [Required]
        public int Quantity { get; set; }

    }
}
