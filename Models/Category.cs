﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EShop.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        public string Name_En { get; set; }

        public string Name_Vn { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
