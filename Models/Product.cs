﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EShop.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        public string Img { get; set; }

        [Required]
        public int UnitPrice { get; set; }

        [Required]
        public DateTime ProductDate { get; set; }

        [Required]
        public bool Available { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }

        [Required]
        public string Description { get; set; }

        internal object ToList()
        {
            throw new NotImplementedException();
        }
    }
}
