﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EShop.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public string Description { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
