﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EShop.Models
{
    public class Customer
    {
        
        public int Id { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

       
        [Required]
        [DataType(DataType.ImageUrl)]
        public string Photo { get; set; }

        [Required]
        public bool Active { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
