﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EShop.Models;

namespace EShop.Data
{
    public class EShopContext : DbContext
    {
        public EShopContext (DbContextOptions<EShopContext> options)
            : base(options)
        {
        }

        public DbSet<EShop.Models.Category> Category { get; set; }

        public DbSet<EShop.Models.Product> Product { get; set; }

        public DbSet<EShop.Models.Order> Order { get; set; }

        public DbSet<EShop.Models.Customer> Customer { get; set; }

        public DbSet<EShop.Models.OrderDetail> OrderDetail { get; set; }
    }
}
