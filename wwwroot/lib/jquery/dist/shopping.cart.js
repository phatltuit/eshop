﻿let cartButton = document.getElementById("cart-dropdown");
let cartContent = document.getElementById("myDropdown");

//Load cart from the beginning
renderCart();

//Cart toggle show
function toggle() {
    if (!cartContent.classList.contains('show')) {
        cartContent.classList.toggle("show");
    } else {
        cartContent.classList.remove("show");
    }
}

//Close the cart if the user clicks outside of it or cart button
window.onclick = function (event) {   
    if (event.target.matches('.dropbtn')) {
        toggle();
    } else if (!$(".dropdown-content")[0].contains(event.target)) {
        cartContent.classList.remove("show");
    }
}


function limitValueInRange(num, min, max) {
    const MIN = min || 0;
    const MAX = max || 99;
    const parsed = parseInt(num)
    return Math.min(Math.max(parsed, MIN), MAX)
}

//Refresh Cart
function renderCart() {
    //Clear cart UI
    cartContent.innerHTML = "";
    var footer = document.createElement("a");
    footer.classList.add("cart-footer");
    footer.innerHTML = "Checkout"
    cartContent.insertAdjacentElement('afterbegin', footer);

    //Populate session storage data back to Cart UI.
    let cart_items = JSON.parse(sessionStorage.getItem("cart_items"));

    cart_items?.forEach(cart_item => {
        //Create Cart elements
        var item_content = document.createElement('a');
        var item_content_img = document.createElement("div");
        var item_content_name = document.createElement("div");
        var item_content_qty = document.createElement("input");
        var item_content_price = document.createElement("div");
        var img = document.createElement("img");

        //set Class
        item_content.setAttribute("id", cart_item.item.id);
        item_content_img.classList.add("item-img");
        item_content_name.classList.add("item-name");
        item_content_price.classList.add("item-price");
        item_content_qty.classList.add("item-qty");
        item_content_qty.setAttribute("type", "number");
        item_content_qty.onchange = function () {
            var oldQty = existItemInCart(cart_item.item.id).qty;
            item_content_qty.value = limitValueInRange(item_content_qty.value, 0, 99);
            updateExistedItemQuantity(cart_item.item.id, item_content_qty.value);
            updateTotalQuantity(item_content_qty.value - oldQty);
            updateTotalPrice((item_content_qty.value - oldQty) * cart_item.item.unitPrice);
        }

        //Set value
        img.setAttribute("src", cart_item.item.img);
        item_content_img.insertAdjacentElement('afterbegin', img);
        item_content_qty.setAttribute("value", cart_item.qty);
        item_content_price.insertAdjacentText("afterbegin", "$" + cart_item.item.unitPrice);     
        item_content_name.insertAdjacentText("afterbegin", cart_item.item.name);  

        //Add in to Cart
        item_content.insertAdjacentElement('afterbegin', item_content_price);
        item_content.insertAdjacentElement('afterbegin', item_content_qty);
        item_content.insertAdjacentElement('afterbegin', item_content_name);
        item_content.insertAdjacentElement('afterbegin', item_content_img);
        
        cartContent.insertAdjacentElement('afterbegin', item_content);
    });
    var header = document.createElement("a");
    header.classList.add("cart-header");
    header.innerHTML = "CART"
    cartContent.insertAdjacentElement('afterbegin', header);   
}

//Ajax request call the "Buy" method controller
function addToCart(id, controllerBuyAction) {
    //Config Ajax Request
    var setting = {
        url: controllerBuyAction,
        data: {
            'id': id
        },
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        beforeSend: function () {
            console.log("Sending !");
        },
        success: function (result) { 
            console.log("Add below product into cart !");
            console.log(result);
            //Add products into cart
            addProductToCart(result["product"]);            
            console.log("Add to Cart successfully !");
        },
        complete: function () {
            console.log("Finish !"); //Finish Adding Product into Cart
        },
        failure: function (jqXHR, textStatus, errorThrown) {
            alert("Status: " + jqXHR.status + "; Error: " + jqXHR.responseText); // Display error message  
        }
    }
    //Send ajax request with config
    $.ajax(setting);
}

//First Initial Shopping Cart
if (sessionStorage.getItem("cart_items") === null) {
    sessionStorage.setItem("cart_items", "[]");
    sessionStorage.setItem("cart_total_qty", "0");
    sessionStorage.setItem("cart_total_price", "0");
}

function isExistItemInCart(id) {
    var cart_Items = JSON.parse(sessionStorage.getItem("cart_items"));
    var existedItem = cart_Items.find(cart_item => cart_item.item.id == id);
    return existedItem != null;
}

function existItemInCart(id) {
    var cart_Items = JSON.parse(sessionStorage.getItem("cart_items"));
    var existedItem = cart_Items.find(cart_item => cart_item.item.id == id);
    return existedItem;
}

function updateExistedItemQuantity(id, amount) {
    var cart_Items = JSON.parse(sessionStorage.getItem("cart_items"));
    cart_Items.find(cart_item => cart_item.item.id == id).qty = amount;
    sessionStorage.setItem("cart_items", JSON.stringify(cart_Items));
}

function updateTotalQuantity(amount) {
    sessionStorage.setItem("cart_total_qty", parseInt(sessionStorage.getItem("cart_total_qty")) + parseInt(amount))
}

function updateTotalPrice(amount) {
    sessionStorage.setItem("cart_total_price", parseInt(sessionStorage.getItem("cart_total_price")) + parseInt(amount))
}

function getTotalQuantity() {
    return sessionStorage.getItem("cart_total_qty");
}

function getTotalPrice() {
    return sessionStorage.getItem("cart_total_price");
}

function addNewCartItems(item) {
    var cart_Items = JSON.parse(sessionStorage.getItem("cart_items"));
    cart_Items.push(item);
    sessionStorage.setItem("cart_items", JSON.stringify(cart_Items));
} 

//Add product into Local Storage
function addProductToCart(product) {
    if (isExistItemInCart(product.id)) {
        updateExistedItemQuantity(product.id, existItemInCart(product.id).qty + 1);   
    } else {
        addNewCartItems({ item: product, qty: 1 });        
    }        
    updateTotalQuantity(1);
    updateTotalPrice(product.unitPrice);
    renderCart();
}

