﻿using EShop.Data;
using EShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;


namespace EShop.Controllers
{
    public class HomeController : Controller
    {
        public readonly EShopContext _context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, EShopContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            var eShopDBContext = _context.Product.Include(p => p.Category);
            return View(eShopDBContext.ToList());
        }


        public JsonResult Buy()
        {
            //Add product to cart
            var id = HttpContext.Request.Query["id"].ToString();            
            var prd = _context.Product.Where(p => p.Id == Convert.ToInt32(id)).First();
            return Json(new {
                product = prd
            });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
